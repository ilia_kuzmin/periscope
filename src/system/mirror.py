from src.geometry import *


class Mirror:
    def __init__(self, triangle: Triangle, platform: Triangle):
        self.static_point = platform.point_a
        self.triangle = triangle
        self.platform = platform

    def get_points_list(self):
        return [self.triangle.point_a, self.triangle.point_b, self.triangle.point_c]

    def get_platform_points(self):
        return [self.platform.point_a, self.platform.point_b, self.platform.point_c]