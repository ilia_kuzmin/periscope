from src.system.periscope import Periscope, Angle
from keras.models import load_model
import numpy
from src.geometry import Point3d
from src.system.periscope import MirrorLocation
import multiprocessing as mp

def add_point_to_list(str_list: [], p: Point3d):
    str_list.append(p.x)
    str_list.append(p.y)
    str_list.append(p.z)


class NeuralNetAlgorithm:
    # def __init__(self):
    #     self.model = load_model('./neuralnet/my_model.h5')
    #
    # # for all in one process implementation
    # def step(self, periscope: Periscope, up, down):
    #     input_list = [periscope.target.location.y, periscope.target.location.z]
    #
    #     angles =  self.model.predict(numpy.array([input_list]))
    #     periscope.mirror_down.triangle = down.rotate_plane(angles[0][0], Angle.YAW)
    #     periscope.mirror_down.triangle = periscope.mirror_down.triangle.rotate_plane(angles[0][1], Angle.PITCH)
    #     periscope.mirror_down.triangle = periscope.mirror_down.triangle.rotate_plane(angles[0][2], Angle.ROLL)
    #
    #     periscope.mirror_up.triangle = up.rotate_plane(angles[0][3], Angle.YAW)
    #     periscope.mirror_up.triangle = periscope.mirror_up.triangle.rotate_plane(angles[0][4], Angle.PITCH)
    #     periscope.mirror_up.triangle = periscope.mirror_up.triangle.rotate_plane(angles[0][5], Angle.ROLL)
    #
    @staticmethod
    def run(self_queue: mp.Queue, arr, arr_pl, periscope: Periscope, plane_loc: MirrorLocation, model):
        down_platform = periscope.mirror_down.platform
        up_platform = periscope.mirror_up.platform

        up_mirror = periscope.mirror_up.triangle
        down_mirror = periscope.mirror_down.triangle
        while True:
            periscope.target = self_queue.get()

            angles = model.predict(numpy.array([[periscope.target.location.y, periscope.target.location.z]]))

            periscope.mirror_down.platform, yaw_ax_down = down_platform.rotate_plane(angles[0][0], Angle.YAW)
            periscope.mirror_down.triangle = down_mirror.rotate_around_ax(periscope.mirror_down.platform.point_a,
                                                                          yaw_ax_down, angles[0][0])

            periscope.mirror_down.platform, pitch_ax_down = periscope.mirror_down.platform.rotate_plane(angles[0][1],
                                                                                                        Angle.PITCH)
            periscope.mirror_down.triangle = \
                periscope.mirror_down.triangle.rotate_around_ax(periscope.mirror_down.platform.point_a,
                                                                pitch_ax_down, angles[0][1])

            periscope.mirror_down.platform, roll_ax_down = periscope.mirror_down.platform.rotate_plane(angles[0][2],
                                                                                                       Angle.ROLL)
            periscope.mirror_down.triangle = \
                periscope.mirror_down.triangle.rotate_around_ax(periscope.mirror_down.platform.point_a,
                                                                roll_ax_down, angles[0][2])


            # ----------------------------------------------------------------------------------------------

            periscope.mirror_up.platform, yaw_ax_up = up_platform.rotate_plane(angles[0][3], Angle.YAW)
            periscope.mirror_up.triangle = up_mirror.rotate_around_ax(periscope.mirror_up.static_point,
                                                                      yaw_ax_up, angles[0][3])

            periscope.mirror_up.platform, pitch_ax_up = periscope.mirror_up.platform.rotate_plane(angles[0][4],
                                                                                                  Angle.PITCH)
            periscope.mirror_up.triangle = \
                periscope.mirror_up.triangle.rotate_around_ax(periscope.mirror_up.static_point,
                                                              pitch_ax_up, angles[0][4])

            periscope.mirror_up.platform, roll_ax_up = periscope.mirror_up.platform.rotate_plane(angles[0][5],
                                                                                                 Angle.ROLL)
            periscope.mirror_up.triangle = \
                periscope.mirror_up.triangle.rotate_around_ax(periscope.mirror_up.static_point,
                                                              roll_ax_up, angles[0][5])

            self_plane = periscope.mirror_down.triangle
            if plane_loc == MirrorLocation.UP:
                self_plane = periscope.mirror_up.triangle

            self_platform = periscope.mirror_down.platform
            if plane_loc == MirrorLocation.UP:
                self_platform = periscope.mirror_up.platform

            arr[0] = self_plane.point_b.x
            arr[1] = self_plane.point_b.y
            arr[2] = self_plane.point_b.z
            arr[3] = self_plane.point_c.x
            arr[4] = self_plane.point_c.y
            arr[5] = self_plane.point_c.z
            arr[6] = self_plane.point_a.x
            arr[7] = self_plane.point_a.y
            arr[8] = self_plane.point_a.z

            arr_pl[0] = self_platform.point_b.x
            arr_pl[1] = self_platform.point_b.y
            arr_pl[2] = self_platform.point_b.z
            arr_pl[3] = self_platform.point_c.x
            arr_pl[4] = self_platform.point_c.y
            arr_pl[5] = self_platform.point_c.z
